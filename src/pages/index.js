import React from "react"
import { Link } from "gatsby"
import Layout from "../components/layout"
import SEO from "../components/seo"
import Main from "../components/Main"
import BigText from "../components/BigText"

const IndexPage = () => {
  return (
    <Layout>
      <SEO title="Home Of D3 JS" />
      <Main
        description="Pure Gatsby Website"
        position="MERN STACK DEV"
        name="Erwin CINO"
      />
      <BigText key="1" text="皆は美人です" />
      <BigText key="2" text="Everyone is Beautiful" />
    </Layout>
  )
}
export default IndexPage
