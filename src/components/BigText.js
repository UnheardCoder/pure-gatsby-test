import React from "react"

const BigText = props => {
  return (
    <div>
      <h1 className="big-text">{props.text}</h1>
    </div>
  )
}

export default BigText
