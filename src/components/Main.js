import React from "react"

const Main = props => {
  return (
    <div>
      <div className="position-main">
        <div className="position-title">
          <p className="name-simple">{props.name}</p>
          <h1 className="osaka">{props.position}</h1>
        </div>
        <div className="beige-lines-div">
          <div className="indi-lines">
            <hr className="beige-lines"></hr>
          </div>
          <div className="indi-lines">
            <hr className="beige-lines-1"></hr>
          </div>
          <div className="indi-lines">
            <hr className="beige-lines-2"></hr>
          </div>
        </div>
        <div className="mini-intro name-simple">
          <p>{props.description}</p>
        </div>
      </div>
    </div>
  )
}

export default Main
